const mod10 = (function(window, document, $) {
  function mod10Rec(ref) {
    const table = [0, 9, 4, 6, 8, 2, 7, 1, 3, 5];
    let carry = 0;

    for (let i = 0; i < ref.length; i++) {
      carry = table[(carry + parseInt(ref.charAt(i))) % 10];
    }
    return (10 - carry) % 10 === 0;
  }
  return {
    mod10Rec
  }
})(window, document, jQuery);
module.exports = mod10;
