const qr = (function(window, document, $) {

  $(() => {
    drawCanvas(38, 388);
  });

  function createQR() {
    const elements = document.getElementById('qr').elements;
    let string;
    string = '';
    for (let i = 0; i < elements.length; i += 1) {
      let value;
      const element = elements[i];
      if (
        element.type !== 'fieldset' &&
        element.type !== 'button' &&
        element.className !== 'onoffswitch-checkbox'&&
        element.type !== 'submit'
      ) {
        switch (element.className.includes('additional')) {
          case true:
            if (element.value === '') {
              break;
            }
          default:
            if (element.name === 'Ustrd' || element.name === 'AltPmt1' || element.name === 'AltPmt2') {
              value = `${element.value}\n`;
              value = value.replace(/\r?\n|\r/g, ' ');
              value += '\n';
            } else {
              value = `${element.value}\n`;
            }
            string += value;
        }
      }
    }
    const options = {
      errorCorrectionLevel: 'M',
    };
    const header = 'SPC\n0100\n1\n';
    let completeString = `${header}${string}`;
    completeString = completeString.trim();
    const canvas = document.getElementById('canvas');
    const image = new Image();
    image.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKYAAACmCAAAAAB146urAAAACXBIWXMAAFxGAABcRgEUlENBAAAAvElEQVR42u3dQQqAIBAFUH90/yvbNghSyMjk/Z0L4eGsHEZMLX/IVjAxMTExMTFvs58XmYpWFR0TExMTExMTExMTE/N3N8sRd8BrouiYmJiYmJiYmJiYmJiYmJiYmJiYb6Srv/lsYra1O4qOiYmJiYmJiYmJiYmJiYmJOXe6ekitLo8ZOUxMTExMTExMTExMTExMTExMTMwPMuQNcJwmJiYmJiYmJiYmJiYmJiYm5srM+HUEExMTExNzCeYBSxwIVP8Vm3EAAAAASUVORK5CYII=';

    QRCode.toCanvas(canvas, completeString, options, (error) => {
      if (error) {
        console.error(error);
      }
      canvas.style.height = '388px';
      canvas.style.width = '388px';
      image.onload = () => {
        const cxt = canvas.getContext('2d');
        /* according to the specification the QR-Code has sides of 46mm. The Swiss-Cross in the middle
        has 7mm sides. To have the correct proportion between the QR-Code and the cross this constant calculates the ratio. */
        const ratio = (7 * 100) / 46;
        image.height = (ratio * canvas.height) / 100;
        image.width = (ratio * canvas.width) / 100;
        cxt.drawImage(
          image,
          (canvas.width / 2) - (image.width / 2),
          (canvas.height / 2) - (image.height / 2),
          image.width,
          image.height,
        );
        const aLink = document.getElementById('saveLink');
        const cdtrName = document.getElementById('CdtrName').value.split(" ").join("");
        aLink.download = `QR-Code-${cdtrName}-${new Date().toISOString()}.png`;
        aLink.href = canvas.toDataURL().replace('image/png', 'image/octet-stream');
        console.log('success!');
      };
    });
  }

  function drawCanvas(start, end) {
    const canvas = document.getElementById('canvas');
    canvas.width = end - start;
    canvas.height = end - start;
    const ctx = canvas.getContext('2d');
    const x = canvas.width;
    const y = canvas.height;
    ctx.beginPath();

    ctx.moveTo(start, start);
    ctx.lineTo(x - start, y - start);
    ctx.stroke();

    ctx.moveTo(x - start, start);
    ctx.lineTo(start, y - start);
    ctx.stroke();
  }

  return {
    createQR,
    drawCanvas
  };
})(window, document, jQuery);
module.exports = qr;
