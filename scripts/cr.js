$(window).on('load', () => {
  changeFieldRequierement($('#UltmtCdtrToggle'));
  changeFieldRequierement($('#UltmtDbtrToggle'));

  changeReferenceRequirement($('#selectRef')[0]);
  checkIfQRIID($('#IBAN')[0]);
  checkReferenceType($('#Ref')[0]);
});

$('#UltmtCdtrToggle').on('keyup change', (event) => {
  changeFieldRequierement(event.currentTarget);
});
$('#UltmtDbtrToggle').on('keyup change', (event) => {
  changeFieldRequierement(event.currentTarget);
});
$('#selectRef').on('change', (event) => {
  changeReferenceRequirement(event.currentTarget);
});
$('#IBAN').on('keyup change', (event) => {
  checkIfQRIID(event.currentTarget);
});
$('#Ref').on('keyup change', (event) => {
  checkReferenceType(event.currentTarget);
});

function changeFieldRequierement(parent) {
  let empty = true;
  let div = $(parent);
  let inputs = $(parent).children('input');
  for (let input of inputs) {
    if (input.value !== '') {
      empty = false;
      break;
    }
  }
  div.children(':input[data-parsley-required]').each(function() {
    if ($(this).attr('data-parsley-required') !== !empty) {
      $(this).attr('data-parsley-required', !empty);
    }
  });
  div.children('span').each(function() {
    if ($(this).hasClass('required') && empty) {
      $(this).toggleClass('required notRequired');
    } else if ($(this).hasClass('notRequired') && !empty) {
      $(this).toggleClass('required notRequired');
    }
  });
}


function changeReferenceRequirement(elem) {
  const span = document.getElementById('reference');
  const ref = document.getElementById('Ref');
  switch (elem.value) {
    case 'NON':
      span.className !== 'notRequired' ? $(span).toggleClass('notRequired required') : span.className;
      $(ref).attr('data-parsley-required', false);
      $(ref).attr('data-parsley-refempty', false);
      $(ref).attr('disabled', 'disabled');
      $(ref).removeAttr('data-parsley-checkcr data-parsley-checkesr data-parsley-type');
      break;
    case 'QRR':
    case 'SCOR':
      span.className !== 'required' ? $(span).toggleClass('notRequired required') : span.className;
      $(ref).attr('data-parsley-required', true);
      $(ref).removeAttr('disabled');
      if ($(`.RmtInf div:last`).hasClass('hidden')) {
        $(`.RmtInf div:last`).toggleClass('hidden shown');
      }
      if (elem.value === 'QRR') {
        $(ref).attr({
          maxlength: '27',
          'data-parsley-type': 'integer'
        });
        $(ref).removeAttr('data-parsley-checkcr data-parsley-refempty');
        $(ref).attr('data-parsley-checkesr', "");
      } else {
        $(ref).attr({
          maxlength: '25',
          'data-parsley-type': 'alphanum'
        });
        $(ref).removeAttr('data-parsley-checkesr data-parsley-refempty');
        $(ref).attr('data-parsley-checkcr', "");
      }
      tg.toggleSwitch(true, `RmtInfonoffswitch`);
      break;
  }
}

function checkIfQRIID(elem) {
  let checkValue;
  checkValue = parseInt(elem.value.substr(4, 5));
  if (30000 <= checkValue && checkValue <= 31999) {
    $('#selectRef').val('QRR');
    $("#selectRef option[value='NON']").remove();
    $('#selectRef').change();
  } else {
    if ($("#selectRef option[value='NON']").length === 0){
      $('#selectRef').append($('<option>', {
        value: 'NON',
        text: 'NON'
      }));
    }
    $('#selectRef').val('NON');
    $('#selectRef').change();
  }
}

function checkReferenceType(elem) {
  let ref = elem.value;
  if (ref !== '' && mod10.mod10Rec(ref)){
    $('#selectRef').val('QRR');
    $('#selectRef').change();
  } else if (ref !== '' && !mod10.mod10Rec(ref)){
    if (iso.checkIfValidISO11649(ref)) {
      $('#selectRef').val('SCOR');
      $('#selectRef').change();
    }
  }
}
