const tg = (function(window, document, $) {
  let StartUltmtCdtrHeight;
  let StartCcyAmtDateHeight;
  let StartUltmtDbtrHeight;
  let StartRmtInfHeight;
  let StartAltPmtHeight;
  let UltmtCdtrHeight;
  let CcyAmtDateHeight;
  let UltmtDbtrHeight;
  let RmtInfHeight;
  let AltPmtHeight;
  $(window).on('load', () => {
    getHeight();
    loopThroughDivs();
    const switches = [
      '#UltmtCdtronoffswitch', '#CcyAmtDateonoffswitch', '#UltmtDbtronoffswitch',
      '#RmtInfonoffswitch', '#AltPmtonoffswitch'
    ];
    switches.forEach((s) => {
      $(s).on('click', function(){
        toggleField(this.parentNode.parentNode.className);
      });
    });
  });

  function getHeight() {
    StartUltmtCdtrHeight = $('#UltmtCdtrToggle').outerHeight(true);
    StartCcyAmtDateHeight = $('#CcyAmtDateToggle').outerHeight(true);
    StartUltmtDbtrHeight = $('#UltmtDbtrToggle').outerHeight(true);
    StartRmtInfHeight = $('#RmtInfToggle').outerHeight(true);
    StartAltPmtHeight = $('#AltPmtToggle').outerHeight(true);
    UltmtCdtrHeight = StartUltmtCdtrHeight;
    CcyAmtDateHeight = StartCcyAmtDateHeight;
    UltmtDbtrHeight = StartUltmtDbtrHeight;
    RmtInfHeight = StartRmtInfHeight;
    AltPmtHeight = StartAltPmtHeight;
  }

  function handleOnLoadDisplay(toggleDiv) {
    let isChecked = false;
    let applyClass = 'hidden';
    let divId = toggleDiv.id;
    let divName = divId.replace('Toggle', '');
    let divToSelect = `.${divName} div:last`;
    let elems = toggleDiv.childNodes;
    let height;
    for (let elem of elems) {
      if (elem.type === 'text' || elem.type === 'textarea') {
        if (elem.value !== '') {
          isChecked = !isChecked;
          applyClass = 'shown';
          $(divToSelect).removeAttr('style');
          if ($(divToSelect).outerHeight() >= 0) {
            $(divToSelect).removeClass('hidden');
            changeDivHeight(divName, divId);
            $(divToSelect).addClass('hidden');
          }
          height = whichHeightVar(divName);
          $(divToSelect).removeClass('hidden');
          $(divToSelect).addClass(applyClass);
          $(`#${divId}`).css('height', height);
          toggleSwitch(isChecked, `${divName}onoffswitch`);
          return;
        }
      }
    }
    if (divId === 'RmtInfToggle' && ($('#selectRef').val() === 'QRR'|| $('#selectRef').val() === 'SCOR')){
      isChecked = !isChecked;
      applyClass = 'shown';
      $(divToSelect).removeAttr('style');
      if ($(divToSelect).outerHeight() >= 0) {
        $(divToSelect).removeClass('hidden');
        changeDivHeight(divName, divId);
        $(divToSelect).addClass('hidden');
      }
      height = whichHeightVar(divName);
      $(divToSelect).removeClass('hidden');
      $(divToSelect).addClass(applyClass);
      $(`#${divId}`).css('height', height);
      toggleSwitch(isChecked, `${divName}onoffswitch`);
      return;
    }
    if ($(`#${divName}onoffswitch`).is(':checked')) {
      changeDivToStartHeight(divName);
    }
    $(divToSelect).removeAttr('style');
    $(divToSelect).removeClass('shown');
    $(divToSelect).addClass(applyClass);
    toggleSwitch(isChecked, `${divName}onoffswitch`);
  }

  function loopThroughDivs() {
    const toggleDivsArray = [
      'UltmtCdtrToggle', 'CcyAmtDateToggle', 'UltmtDbtrToggle',
      'RmtInfToggle', 'AltPmtToggle'
    ];
    toggleDivsArray.forEach((id) => {
      const elem = document.getElementById(id);
      handleOnLoadDisplay(elem);
    });
  }

  function toggleField(className) {
    let height = whichHeightVar(className);
    let div = `.${className} div:last`;
    let divClass = `.${className} .shown`;
    $(div).toggleClass('hidden shown');
    $(div).attr('class') === 'shown' ? $(divClass).css('height', height) : $(div).removeAttr('style');
  }

  function toggleSwitch(value, id) {
    let s = document.getElementById(id);
    s.checked = value;
  }

  function changeDivHeight(className, divId) {
    switch (className) {
      case 'UltmtCdtr':
        UltmtCdtrHeight = $(`#${divId}`).outerHeight(true);
        break;
      case 'CcyAmtDate':
        CcyAmtDateHeight = $(`#${divId}`).outerHeight(true);
        break;
      case 'UltmtDbtr':
        UltmtDbtrHeight = $(`#${divId}`).outerHeight(true);
        break;
      case 'RmtInf':
        RmtInfHeight = $(`#${divId}`).outerHeight(true);
        break;
      case 'AltPmt':
        AltPmtHeight = $(`#${divId}`).outerHeight(true);
        break;
    }
  }

  function changeDivToStartHeight(className) {
    switch (className) {
      case 'UltmtCdtr':
        UltmtCdtrHeight = StartUltmtCdtrHeight;
        break;
      case 'CcyAmtDate':
        CcyAmtDateHeight = StartCcyAmtDateHeight;
        break;
      case 'UltmtDbtr':
        UltmtDbtrHeight = StartUltmtDbtrHeight;
        break;
      case 'RmtInf':
        RmtInfHeight = StartRmtInfHeight;
        break;
      case 'AltPmt':
        AltPmtHeight = StartAltPmtHeight;
        break;
    }
  }

  function whichHeightVar(className) {
    let height;
    switch (className) {
      case 'UltmtCdtr':
        height = UltmtCdtrHeight;
        break;
      case 'CcyAmtDate':
        height = CcyAmtDateHeight;
        break;
      case 'UltmtDbtr':
        height = UltmtDbtrHeight;
        break;
      case 'RmtInf':
        height = RmtInfHeight;
        break;
      case 'AltPmt':
        height = AltPmtHeight;
        break;
    }
    return height;
  }

  return {
    getHeight,
    loopThroughDivs,
    toggleField,
    toggleSwitch
  };
})(window, document, jQuery);
module.exports = tg;
