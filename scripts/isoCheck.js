const iso = (function(window, document, $) {
  function checkIfISO3166Code(value) {
    for (let entry of codes) {
      if (entry.Code === value) {
        return true;
      }
    }
    return false;
  }
  function checkIfValidISO11649(value) {
    // Translation of chars into digits
    const charTable = {
      'A': 10, 'B': 11, 'C': 12, 'D': 13, 'E': 14, 'F': 15,
      'G': 16, 'H': 17, 'I': 18, 'J': 19, 'K': 20, 'L': 21,
      'M': 22, 'N': 23, 'O': 24, 'P': 25, 'Q': 26, 'R': 27,
      'S': 28, 'T': 29, 'U': 30, 'V': 31, 'W': 32, 'X': 33,
      'Y': 34, 'Z': 35
    };
    let ref = '';
    let num = '';
    ref = value.toUpperCase();
    // move the first 4 chars to the end of the string
    ref = ref.substr(4) + ref.substr(0, 4);
    // Loop through the string and replace chars to numeric
    for (let i = 0; i < ref.length; i++) {
      if (charTable[ref.charAt(i)] === undefined) {
        num += ref.substr(i, 1);
      } else {
        num += ref.substr(i, 1).replace(ref.charAt(i), charTable[ref.charAt(i)]);
      }
    }
    // creditor reference is valid if max 25 chars and mod 97 remainder is 1
    return (ref.length < 26) && (IBAN.mod97(num) === 1);
  }
  return {
    checkIfISO3166Code,
    checkIfValidISO11649
  }
})(window, document, jQuery);
module.exports = iso;
