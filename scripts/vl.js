$(window).on('load', () => {
  window.Parsley.addValidator('checkiban', {
    validateString: function(value) {
      return IBAN.isValidIBANNumber(value);
    },
    messages: {
      en: 'This is not a valid IBAN.',
      de: 'Das ist keine gültige IBAN.'
    }
  });
  window.Parsley.addValidator('countrycode', {
    validateString: function(value) {
      return iso.checkIfISO3166Code(value);
    },
    messages: {
      en: 'This is not a valid Country-Code.',
      de: 'Das ist keine gültiger Landescode.'
    }
  });

  window.Parsley.addValidator('checkesr', {
    validateString: function(value) {
      return mod10.mod10Rec(value);
    },
    messages: {
      en: 'This is not a valid QRR.',
      de: 'Das ist kein gültiger QRR.'
    }
  });
  window.Parsley.addValidator('checkcr', {
    validateString: function(value) {
      return iso.checkIfValidISO11649(value);
    },
    messages: {
      en: 'This is not a valid Creditor Reference.',
      de: 'Das ist kein gültige Kreditoren-Referenz.'
    }
  });
  window.Parsley.addValidator('refempty', {
    validateString: function(value) {
      return $('#Ref').val() === '';
    },
    messages: {
      en: 'This field needs to be empty.',
      de: 'Dieses Feld muss leer bleiben.'
    }
  });
  const formInstance = $('form').parsley();
  $('#qrButton').on('click', () => {
    formInstance.validate();
  });
  formInstance.on('form:validated', (formInstance) => {
    tg.loopThroughDivs();
    if (formInstance.isValid()) {
      qr.createQR();
      $('form').one('keyup', () => {
        qr.drawCanvas(38, 388);
      });
    }
  });
});
