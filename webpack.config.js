const webpack = require('webpack');
const node_dir = __dirname + '/node_modules';

module.exports = {
  entry: {
    entry: __dirname + '/scripts/index.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: __dirname + '/bundle/',
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      comments: false,
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      Parsley: "parsley",
      parsleyjs: "parsley",
      parsley: "parsley",
      "window.Parsley": "parsley",
      QRCode: "qrcode",
      tg: "tg",
      IBAN: "IBAN",
      vl: "vl",
      cr: "cr",
      codes: "codes",
      iso: "iso",
      mod10: "mod10",
      qr: "qr"
    })
  ],
  resolve: {
    alias: {
      jquery: node_dir + "/jquery/dist/jquery.min.js",
      parsley: node_dir + "/parsleyjs/dist/parsley.min.js",
      qrcode: node_dir + "/qrcode/build/qrcode.min.js",
      tg: __dirname + "/scripts/tg.js",
      IBAN: __dirname + "/scripts/ibanCheck.js",
      vl: __dirname + '/scripts/vl.js',
      cr: __dirname +'/scripts/cr.js',
      codes: __dirname + '/scripts/iso3166-1.js',
      iso: __dirname + '/scripts/isoCheck.js',
      mod10: __dirname + '/scripts/mod10.js',
      qr: __dirname + '/scripts/qr.js',
    }
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
}
